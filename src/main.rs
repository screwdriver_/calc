use std::io;

mod lexer;
mod ast;

// TODO Handle integer compute
fn compute(cmd: &String, tokens: &Vec::<lexer::Token>, n: &ast::AST) -> f64 {
	let token = &tokens[n.get_token()];
	let children = n.get_children();
	match n.get_type() {
		ast::NodeType::Expression => {
			compute(cmd, tokens, &children[0])
		},
		ast::NodeType::Value => {
			token.get_str(cmd).parse::<f64>().unwrap()
		},
		ast::NodeType::Add => {
			compute(cmd, tokens, &children[0]) + compute(cmd, tokens, &children[1])
		},
		ast::NodeType::Sub => {
			compute(cmd, tokens, &children[0]) - compute(cmd, tokens, &children[1])
		},
		ast::NodeType::Mul => {
			compute(cmd, tokens, &children[0]) * compute(cmd, tokens, &children[1])
		},
		ast::NodeType::Div => {
			compute(cmd, tokens, &children[0]) / compute(cmd, tokens, &children[1])
		},
		ast::NodeType::Mod => {
			(compute(cmd, tokens, &children[0]) as i64
				% compute(cmd, tokens, &children[1]) as i64) as f64
		},
		/*FuncCall => {
			// TODO
		},*/
		_ => 0., // TODO Error
	}
}

fn exec_command(cmd: &String) {
	let tokens = lexer::tokenize(cmd);
	/*println!("Tokens:");
	for (_, t) in tokens.iter().enumerate() {
		println!("- {} -> {}", t, t.get_str(cmd));
	}*/

	let ast = ast::parse(&tokens);
	/*println!("AST:");
	println!("{}", ast);*/

	println!("= {}", compute(cmd, &tokens, &ast));
}

fn main() {
	// TODO Check if stdin is interative (argument can override it)

	let mut line = String::new();
    let stdin = io::stdin();

    while let Ok(_) = stdin.read_line(&mut line) {
        exec_command(&line);
		line.clear();
    }
}
