use crate::lexer;
use std::fmt;

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum NodeType {
	Expression,
	Value,
	Add,
	Sub,
	Mul,
	Div,
	Mod,
	FuncCall,
	Unknown,
}

pub struct AST {
	type_: NodeType,
	token: usize,
	children: Vec::<AST>,
}

fn print_tabs(n: usize, f: &mut fmt::Formatter<'_>) {
	for _ in 0..n {
		write!(f, "\t");
	}
}

impl AST {
	pub fn new(type_: NodeType, token: usize) -> Self {
		Self {
			type_: type_,
			token: token,
			children: Vec::<AST>::new(),
		}
	}

	fn print(&self, level: usize, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		print_tabs(level, f);
		write!(f, "- {:?}\n", self.type_)?;
		for (_, c) in self.children.iter().enumerate() {
			c.print(level + 1, f)?;
		}
		Ok(())
	}

	pub fn get_type(&self) -> NodeType {
		self.type_
	}

	pub fn get_token(&self) -> usize {
		self.token
	}

	pub fn get_children(&self) -> &Vec::<AST> {
		&self.children
	}
}

impl fmt::Display for AST {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		self.print(0, f)
	}
}

fn token_type_to_node_type(t: lexer::TokenType) -> NodeType {
	match t {
		lexer::TokenType::OpAdd => NodeType::Add,
		lexer::TokenType::OpSub => NodeType::Sub,
		lexer::TokenType::OpMul => NodeType::Mul,
		lexer::TokenType::OpDiv => NodeType::Div,
		lexer::TokenType::OpMod => NodeType::Mod,
		_ => NodeType::Unknown,
	}
}

fn parse_function_call(tokens: &[lexer::Token], i: &mut usize) -> AST {
	let node = AST::new(NodeType::FuncCall, *i);
	*i += 1;

	// TODO Check for open brace
	// TODO Take every argument until closing brace, separated by comma
	// TODO If no closing brace, unexpected EOF
	node
}

fn parse_expression(tokens: &[lexer::Token], i: &mut usize) -> AST {
	let mut node = AST::new(NodeType::Expression, *i);
	*i += 1;
	node.children.push(parse_(tokens, i));
	// TODO Check for brace close
	node
}

fn parse_operand(tokens: &[lexer::Token], i: &mut usize) -> AST {
	let t = tokens[*i].get_type();
	if t == lexer::TokenType::Name {
		return parse_function_call(tokens, i);
	} else if t == lexer::TokenType::BraceOpen {
		return parse_expression(tokens, i);
	}

	let n = match t {
		lexer::TokenType::Integer => AST::new(NodeType::Value, *i),
		lexer::TokenType::Float => AST::new(NodeType::Value, *i),
		_ => AST::new(NodeType::Unknown, *i),
	};
	*i += 1;
	n
}

fn parse_factors(tokens: &[lexer::Token], i: &mut usize) -> AST {
	let mut n = parse_operand(tokens, i);
	while *i < tokens.len() && tokens[*i].get_type() != lexer::TokenType::BraceClose {
		let token_type = tokens[*i].get_type();
		let type_ = token_type_to_node_type(tokens[*i].get_type());
		if token_type != lexer::TokenType::OpMul
			&& token_type != lexer::TokenType::OpDiv
			&& token_type != lexer::TokenType::OpMod
			&& token_type != lexer::TokenType::BraceOpen {
			break;
		}

		if token_type != lexer::TokenType::BraceOpen {
			let mut op_node = AST::new(type_, *i);
			op_node.children.push(n);
			n = op_node;
			*i += 1;
		} else {
			let mut op_node = AST::new(NodeType::Mul, *i);
			op_node.children.push(n);
			n = op_node;
		}
		n.children.push(parse_operand(tokens, i));
	}
	n
}

fn parse_sums(tokens: &[lexer::Token], i: &mut usize) -> AST {
	let mut n = parse_factors(tokens, i);
	while *i < tokens.len() && tokens[*i].get_type() != lexer::TokenType::BraceClose {
		let token_type = tokens[*i].get_type();
		if token_type != lexer::TokenType::OpAdd
			&& token_type != lexer::TokenType::OpSub {
			break;
		}

		let type_ = token_type_to_node_type(tokens[*i].get_type());
		let mut op_node = AST::new(type_, *i);
		op_node.children.push(n);
		n = op_node;

		*i += 1;
		n.children.push(parse_factors(tokens, i));
	}
	n
}

fn parse_(tokens: &[lexer::Token], i: &mut usize) -> AST {
	parse_sums(tokens, i)
}

pub fn parse(tokens: &[lexer::Token]) -> AST {
	let mut i = 0;
	let ast = parse_(tokens, &mut i);
	if i != tokens.len() {
		// TODO Error
	}
	ast
}
