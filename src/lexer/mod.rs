use std::fmt;

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum TokenType {
	BraceOpen,
	BraceClose,
	Integer,
	Float,
	Name,
	OpAdd,
	OpSub,
	OpMul,
	OpDiv,
	OpMod,
	OpComma,
	Unknown,
}

pub struct Token {
	offset: usize,
	size: usize,
	type_: TokenType,
}

impl Token {
	pub fn new(offset: usize, size: usize, type_: TokenType) -> Self {
		Self {
			offset: offset,
			size: size,
			type_: type_,
		}
	}

	pub fn get_offset(&self) -> usize {
		self.offset
	}

	pub fn get_size(&self) -> usize {
		self.size
	}

	pub fn get_type(&self) -> TokenType {
		self.type_
	}

	pub fn get_str<'a>(&self, cmd: &'a String) -> &'a str {
		&cmd[self.offset..(self.offset + self.size)]
	}
}

impl fmt::Display for Token {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "{} {} {:?}", self.offset, self.size, self.type_)
	}
}

fn skip_spaces(cmd: &[u8], i: usize) -> usize {
	let mut j = i;
	while j < cmd.len() && (cmd[j] as char).is_whitespace() {
		j += 1;
	}
	j
}

fn handle_name(cmd: &[u8], i: usize) -> Token {
	let mut j = i;
	while (cmd[j] as char).is_ascii_alphabetic() {
		j += 1;
	}

	Token::new(i, j - i, TokenType::Name)
}

fn handle_number(cmd: &[u8], i: usize) -> Token {
	let mut is_float = false;
	let mut j = i;

	let c = cmd[i] as char;
	if c == '+' || c == '-' {
		j += 1;
	}

	while j < cmd.len() {
		let c = cmd[j] as char;
		if c == '.' {
			if is_float {
				break;
			}
			is_float = true;
		} else if !c.is_ascii_digit() {
			break;
		}
		j += 1;
	}

	if is_float {
		Token::new(i, j - i, TokenType::Float)
	} else {
		Token::new(i, j - i, TokenType::Integer)
	}
}

fn handle_other_token(cmd: &[u8], i: usize) -> Token {
	let c = cmd[i] as char;
	if c == '+' || c == '-' {
		if i + 1 < cmd.len()
			&& ((cmd[i + 1] as char).is_ascii_digit() || (cmd[i + 1] as char) == '.') {
			return handle_number(cmd, i);
		} else if c == '+' {
			return Token::new(i, 1, TokenType::OpAdd);
		} else if c == '-' {
			return Token::new(i, 1, TokenType::OpSub);
		}
	} else if c.is_ascii_digit() || c == '.' {
		return handle_number(cmd, i);
	} else if c.is_ascii_alphabetic() {
		return handle_name(cmd, i);
	}
	Token::new(i, 1, TokenType::Unknown)
}

pub fn tokenize(cmd: &String) -> Vec::<Token> {
	let bytes = cmd.as_bytes();
	let mut tokens = Vec::<Token>::new();
	let mut i = 0;
	while i < bytes.len() {
		i = skip_spaces(&bytes, i);
		if i >= bytes.len() {
			break;
		}

		let c = bytes[i] as char;
		if tokens.len() > 0 {
			let t = tokens[tokens.len() - 1].get_type();
			if (t == TokenType::Integer || t == TokenType::Float) && (c == '+' || c == '-') {
				match c {
					'+' => tokens.push(Token::new(i, 1, TokenType::OpAdd)),
					'-' => tokens.push(Token::new(i, 1, TokenType::OpSub)),
					_ => {}
				}
				i += 1;
				continue;
			}
		}

		match c {
			'(' => {
				tokens.push(Token::new(i, 1, TokenType::BraceOpen));
				i += 1;
			},
			')' => {
				tokens.push(Token::new(i, 1, TokenType::BraceClose));
				i += 1;
			},
			'*' => {
				tokens.push(Token::new(i, 1, TokenType::OpMul));
				i += 1;
			},
			'/' => {
				tokens.push(Token::new(i, 1, TokenType::OpDiv));
				i += 1;
			},
			'%' => {
				tokens.push(Token::new(i, 1, TokenType::OpMod));
				i += 1;
			},
			',' => {
				tokens.push(Token::new(i, 1, TokenType::OpComma));
				i += 1;
			},
			_ => {
				let t = handle_other_token(&bytes, i);
				tokens.push(t);
				i += tokens[tokens.len() - 1].size;
			},
		}
	}
	tokens
}
